app.controller('brandController', function($scope, $controller, brandService) {
	$controller('baseController', {
		$scope : $scope
	});// 继承

	// 进行分页
	$scope.findPage = function(page, rows) {
		brandService.findPage(page, rows).success(function(response) {
			$scope.list = response.rows;
			$scope.paginationConf.totalItems = response.total;
		});
	}

	// 保存
	$scope.save = function() {
		var object = null;
		if ($scope.entity.id != null) {
			object = brandService.update($scope.entity);
		} else {
			object = brandService.add($scope.entity);
		}
		object.success(function(response) {
			if (response.success) {
				// 添加成功,重新刷新列表
				$scope.reloadList();
			} else {
				alert(response.message);
			}
		})
	}

	// 根据Id查询一个
	$scope.findOne = function(id) {
		brandService.findOne(id).success(function(response) {
			$scope.entity = response;
		})
	}

	// 批量删除
	$scope.del = function() {
		// 获取选中的复选框
		brandService.del($scope.selectIds).success(function(response) {
			if (response.success) {
				$scope.reloadList();// 刷新列表
			}
		})
	}

	$scope.searchEntity = {};
	// 进行搜索
	$scope.search = function(page, rows) {
		brandService.search(page, rows, $scope.searchEntity).success(
				function(response) {
					$scope.list = response.rows;
					$scope.paginationConf.totalItems = response.total;
				});
	}
});