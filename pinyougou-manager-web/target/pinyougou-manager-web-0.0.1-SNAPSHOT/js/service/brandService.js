app.service('brandService', function($http){
    		this.del=function(selectIds){
    			return $http.get('../brand/delete.do?ids='+selectIds);
    		}
    		this.search=function(page, rows, searchEntity){
    			return $http.post('../brand/search.do?page='+page+'&rows='+rows, searchEntity);
    		}
    		this.findPage=function(page, rows){
    			return $http.get('../brand/findPage.do?page='+page+'&rows='+rows);
    		}
    		this.findOne=function(id){
    			return $http.get('../brand/findOne.do?id='+id);
    		}

    		this.add=function(entity){
    			return $http.post('../brand/add.do', entity);
    		}

    		this.update=function(entity){
    			return $http.post('../brand/update.do', entity);
    		}
    		//获取下拉列表
    		this.selectOptionList=function(){
    			return $http.get('../brand/selectOptionList.do?');
    		}
    	});