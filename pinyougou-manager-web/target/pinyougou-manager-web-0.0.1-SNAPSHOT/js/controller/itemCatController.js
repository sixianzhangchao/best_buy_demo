//控制层 
app.controller('itemCatController', function($scope, $controller,
		itemCatService,typeTemplateService) {

	$controller('baseController', {
		$scope : $scope
	});// 继承

	// 读取列表数据绑定到表单中
	$scope.findAll = function() {
		itemCatService.findAll().success(function(response) {
			$scope.list = response;
		});
	}

	// 分页
	$scope.findPage = function(page, rows) {
		itemCatService.findPage(page, rows).success(function(response) {
			$scope.list = response.rows;
			$scope.paginationConf.totalItems = response.total;// 更新总记录数
		});
	}

	// 查询实体
	$scope.findOne = function(id) {
		itemCatService.findOne(id).success(function(response) {
			$scope.entity = response;
		});
	}

	// 保存
	$scope.save = function() {
		var serviceObject;// 服务层对象
		if ($scope.entity.id != null) {// 如果有ID
			serviceObject = itemCatService.update($scope.entity); // 修改
		} else {
			$scope.entity.parentId = $scope.parentId;//给传向后台的entity的parentId赋值
			serviceObject = itemCatService.add($scope.entity);// 增加
		}
		serviceObject.success(function(response) {
			if (response.success) {
				// 重新查询
				$scope.findByParentId($scope.parentId);// 重新加载
			} else {
				alert(response.message);
			}
		});
	}

	// 批量删除
	$scope.dele = function() {
		//判断是否还有子级目录
		/*if(true){*/
		// 获取选中的复选框
		if (confirm("确认删除所选吗?")){
			//对selectIds进行判断,有就执行操作,没有就提示用户进行选择
			if ($scope.selectIds != null && $scope.selectIds.length > 0){
				itemCatService.dele($scope.selectIds).success(function(response) {
					/*if (response.success) {*/
						// 重新查询
						$scope.findByParentId($scope.parentId);// 重新加载
						$scope.selectIds = [];
						alert(response.message);
					/*}else{
						alert("该商品分类下还有其他分类,请删除其下所有分类后再试");
					}*/
				});
			}else{
				//没有选择删除的条目提示的信息
				alert("请选择需要删除的条目!");
			}			
		}
	}

	$scope.searchEntity = {};// 定义搜索对象

	// 搜索
	$scope.search = function(page, rows) {
		itemCatService.search(page, rows, $scope.searchEntity).success(
				function(response) {
					$scope.list = response.rows;
					$scope.paginationConf.totalItems = response.total;// 更新总记录数
				});
	}

	$scope.parentId=0;//上级Id
	// 根据iD查询列表
	$scope.findByParentId = function(parentId) {
		$scope.parentId = parentId;//记录上级Id
		itemCatService.findByParentId(parentId).success(function(rensponse) {
			$scope.list = rensponse
		})
	}

	// 面包屑导航
	$scope.grade = 1;

	$scope.setGrade = function(value) {
		$scope.grade = value;
	}

	// 读取列表
	$scope.selectList = function(p_entity) {
		// 如果是1级目录,则下面都是空
		if ($scope.grade == 1) {
			$scope.entity_1 = null;
			$scope.entity_2 = null;
		}
		// 如果是2级目录,那么1级和2级目录有内容,3级目录为空
		if ($scope.grade == 2) {
			$scope.entity_1 = p_entity;
			$scope.entity_2 = null;
		}
		// 如果是三级目录,那么3级目录有内容
		if ($scope.grade == 3) {
			$scope.entity_2 = p_entity;
		}

		$scope.findByParentId(p_entity.id);// 查询此目录的下级目录列表
	}
	
	// 定义类型模板下拉列表
	$scope.typeList={data:[]};//品牌列表
    
	//获取类型模板下拉列表
	$scope.findTypeList=function(){
		typeTemplateService.selectOptionList().success(
			function(response){
				$scope.typeList={data:response};
			}
		)
	}
});
