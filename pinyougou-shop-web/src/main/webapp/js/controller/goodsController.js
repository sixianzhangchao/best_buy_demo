//控制层 
app.controller('goodsController', function($scope, $controller, $location, goodsService,
		uploadService, itemCatService, typeTemplateService) {

	$controller('baseController', {
		$scope : $scope
	});// 继承

	//上架
	$scope.upperShelf = function(){
		// 获取选中的复选框
		goodsService.upperShelf($scope.selectIds).success(function(response) {
			if (response.success) {
				$scope.reloadList();// 刷新列表
				$scope.selectIds = [];
			}
		});
	}
	
	//下架
	$scope.lowerShelf = function(){
		// 获取选中的复选框
		goodsService.lowerShelf($scope.selectIds).success(function(response) {
			if (response.success) {
				$scope.reloadList();// 刷新列表
				$scope.selectIds = [];
			}
		});
	}
	
	
	// 读取列表数据绑定到表单中
	$scope.findAll = function() {
		goodsService.findAll().success(function(response) {
			$scope.list = response;
		});
	}

	// 分页
	$scope.findPage = function(page, rows) {
		goodsService.findPage(page, rows).success(function(response) {
			$scope.list = response.rows;
			$scope.paginationConf.totalItems = response.total;// 更新总记录数
		});
	}

	// 查询实体
	$scope.findOne = function() {
		var id = $location.search()['id'];//获取地址栏参数
		if(id == null){
			return ;
		}
		goodsService.findOne(id).success(function(response) {
			$scope.entity = response;
			
			//向富文本编辑器添加商品介绍
			editor.html($scope.entity.goodsDesc.introduction);
			//读取图片
			$scope.entity.goodsDesc.itemImages = JSON.parse($scope.entity.goodsDesc.itemImages);
			
			// 显示扩展属性
			$scope.entity.goodsDesc.customAttributeItems=
			JSON.parse($scope.entity.goodsDesc.customAttributeItems);
			
			//SKU列表规格列转换
			for(var i = 0; i< $scope.entity.itemList.length; i++){
				$scope.entity.itemList[i].spec = JSON.parse(
						$scope.entity.itemList[i].spec);
			}
		});
	}

	// 保存
	$scope.save = function() {
		//提取文本编辑器的值
		$scope.entity.goodsDesc.introduction=editor.html(); 
		
		var serviceObject;// 服务层对象
		if ($scope.entity.goods.id != null) {// 如果有ID,还可以使用$location.search()['id']获取id
			serviceObject = goodsService.update($scope.entity); // 修改
		} else {
			serviceObject = goodsService.add($scope.entity);// 增加
		}
		serviceObject.success(function(response) {
			if (response.success) {
				location.href='goods.html';//跳转到商品详情页
			} else {
				alert(response.message);
			}
		});
	}

	// 保存
	$scope.add = function() {
		// 获取富文本框中的html
		$scope.entity.goodsDesc.introduction = editor.html();
		goodsService.add($scope.entity).success(function(response) {
			if (response.success) {
				alert("保存成功");
				// 将文件框内容重置
				$scope.entity = {};
				editor.html('');// 清空富文本编辑器
			} else {
				alert(response.message);
			}
		});
	}

	// 批量删除
	$scope.dele = function() {
		// 获取选中的复选框
		goodsService.dele($scope.selectIds).success(function(response) {
			if (response.success) {
				$scope.reloadList();// 刷新列表
				$scope.selectIds = [];
			}
		});
	}

	$scope.searchEntity = {};// 定义搜索对象

	// 搜索
	$scope.search = function(page, rows) {
		goodsService.search(page, rows, $scope.searchEntity).success(
				function(response) {
					$scope.list = response.rows;
					$scope.paginationConf.totalItems = response.total;// 更新总记录数
				});
	}

	// 上传图片
	$scope.uploadFile = function() {
		uploadService.uploadFile().success(function(response) {
			if (response.success) {// 如果上传成功，取出 url
				$scope.image_entity.url = response.message;// 设置文件地址
			} else {
				alert(response.message);
			}
		}).error(function() {
			alert("上传发生错误");
		});
	};

	
	// 定义组合实体类
	$scope.entity = {
		goodsDesc : {
			itemImages : [],
			specificationItems : []
		}
	};
	// 添加图片列表
	$scope.add_image_entity = function() {
		$scope.entity.goodsDesc.itemImages.push($scope.image_entity);
	}

	// 从列表中移出图,删除图片列表中的图片
	$scope.remove_image_entity = function(index) {
		$scope.entity.goodsDesc.itemImages.splice(index, 1);
	}

	// 读取一级分类
	$scope.selectItemCat1List = function() {
		itemCatService.findByParentId(0).success(function(response) {
			$scope.itemCat1List = response;
		})
	}

	// 读取二级分类下拉列表
	$scope.$watch('entity.goods.category1Id', function(newValue, oldValue) {
		// 根据选择的值,查询二级分类
		itemCatService.findByParentId(newValue).success(function(response) {
			$scope.itemCat2List = response;
		})
	})

	// 读取三级分类下拉列表
	$scope.$watch('entity.goods.category2Id', function(newValue, oldValue) {
		// 根据选择的值,查询二级分类
		itemCatService.findByParentId(newValue).success(function(response) {
			$scope.itemCat3List = response;
		})
	})

	// 读取模板Id
	$scope.$watch('entity.goods.category3Id', function(newValue, oldValue) {
		itemCatService.findOne(newValue).success(function(response) {
			$scope.entity.goods.typeTemplateId = response.typeId;// 更新模板Id
		})
	})

	// 模板Id确定之后 更新品牌列表
	$scope.$watch('entity.goods.typeTemplateId', function(newValue, oldValue) {
		// 模板Id确定之后更新模板
		typeTemplateService.findOne(newValue).success(
				function(response) {
					$scope.typeTemplate = response;// 获取类型模板

					$scope.typeTemplate.brandIds = JSON
							.parse($scope.typeTemplate.brandIds);// 品牌列表
					if($location.search()['id'] == null){//这条数据会覆盖,增加判断,如果是新增,就执行
						$scope.entity.goodsDesc.customAttributeItems = JSON
						.parse($scope.typeTemplate.customAttributeItems)// 扩展属性
					}
					$scope.entity.goodsDesc.specificationItems=JSON.parse(
							$scope.entity.goodsDesc.specificationItems);
				})
		// 规格列表
		typeTemplateService.findSpecList(newValue).success(function(response) {
			$scope.specList = response;
		})
	})
	
	//根据规格名称和选项名称返回是否被勾选
	$scope.checkAttributeValue=function(specName,optionName){
		var items = $scope.entity.goodsDesc.specificationItems;
		var object= $scope.searchObjectByKey(items,'attributeName',specName);
		if(object == null){
			return false;
		}else{
			if(object.attributeValue.indexOf(optionName)>=0){
				return true;
			}else{
				return false;
			}
		}
	}

	// 保存选中的规格选项
	$scope.updateSpecAttribute = function($event, name, value) {

		var object = $scope.searchObjectByKey(
				$scope.entity.goodsDesc.specificationItems, 'attributeName',
				name);

		if (object != null) { // 选中
			if ($event.target.checked) {
				object.attributeValue.push(value);
			} else {// 取消勾选
				object.attributeValue.splice(object.attributeValue
						.indexOf(value), 1);// 移除选项
				// 如果选项都取消了,将此条记录删除
				if (object.attributeValue.length == 0) {
					$scope.entity.goodsDesc.specificationItems.splice(
							$scope.entity.goodsDesc.specificationItems
									.indexOf(object), 1);
				}
			}
		} else {
			$scope.entity.goodsDesc.specificationItems.push({
				"attributeName" : name,
				"attributeValue" : [ value ]
			});
		}
	}

	// 创建SKU列表
	$scope.createItemList = function() {
		$scope.entity.itemList = [ {
			spec : {},
			price : 0,
			num : 99999,
			status : '0',
			isDefault : '0'
		} ];// 初始
		var items = $scope.entity.goodsDesc.specificationItems;
		for (var i = 0; i < items.length; i++) {
			$scope.entity.itemList = addColumn($scope.entity.itemList,
					items[i].attributeName, items[i].attributeValue);
		}
	}
	// 添加列值
	addColumn = function(list, columnName, conlumnValues) {
		var newList = [];// 新的集合
		for (var i = 0; i < list.length; i++) {
			var oldRow = list[i];
			for (var j = 0; j < conlumnValues.length; j++) {
				var newRow = JSON.parse(JSON.stringify(oldRow));// 深克隆
				newRow.spec[columnName] = conlumnValues[j];
				newList.push(newRow);
			}
		}
		return newList;
	}
	
	$scope.shelf=['未上架','上架'];
	
	//商品状态
	$scope.status=['未审核','已审核','审核未通过','关闭'];
	
	//商品分类列表(包含1级分类,2级分类,3级分类)
	$scope.itemCatList=[];
	
	//加载商品分类列表
	$scope.findItemCatList=function(){
		itemCatService.findAll().success(
			function(response){
				for(var i = 0; i < response.length; i++){
					$scope.itemCatList[response[i].id] = response[i].name;
				}
			}	
		)
	}
});
