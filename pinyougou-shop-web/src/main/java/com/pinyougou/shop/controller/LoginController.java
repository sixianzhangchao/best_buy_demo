package com.pinyougou.shop.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@RequestMapping("/name")
	public Map<String, String> name() {
		// 从SpringContext中获取name
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		// 建立map集合,将获取到name放到map集合中
		Map<String, String> map = new HashMap<>();
		map.put("loginName", name);
		return map;
	}
}
