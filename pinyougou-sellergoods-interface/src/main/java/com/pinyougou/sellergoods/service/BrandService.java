package com.pinyougou.sellergoods.service;

import java.util.List;
import java.util.Map;

import com.pinyougou.pojo.TbBrand;

import entity.PageResult;
import entity.Result;

/**
 * 品牌接口
 * @author 20149
 *
 */
public interface BrandService {
	/**
	 * 查找所有品牌
	 * @return
	 */
	public List<TbBrand> findAll();
	
	/**
	 * 分页查询
	 * @param page
	 * @param size
	 * @return
	 */
	public PageResult findPage(int page,int size);
	
	/**
	 * 添加品牌
	 * @param tbBrand
	 */
	public void add(TbBrand tbBrand); 
	
	/**
	 * 删除
	 * @param ids
	 */
	public void delete(long[] ids);

	/**
	 * 带条件的分页
	 * @param tbBrand
	 * @param page
	 * @param rows
	 * @return
	 */
	public PageResult findPage(TbBrand tbBrand, int page, int rows);

	/**
	 * 根据Id查找
	 * @param id
	 * @return
	 */
	public TbBrand findOne(Long id);
	
	/**
	 *  更新品牌
	 * @param tbBrand
	 * @return
	 */
	public Result update(TbBrand tbBrand);
	
	/**
	 * 获取品牌下拉列表
	 * @return
	 */
	public List<Map> selectOptionList();
}
